#include "ros_camera_wrapper/ros_camera_wrapper.hpp"

RosCameraWrapper::RosCameraWrapper()
{
    // Create a node handle
    _nh = ros::NodeHandle("~");

    _nh.param<int>("fps", _fps, 30);
    _nh.param<int>("desired_image_width", _desired_image_width, 640);
    _nh.param<int>("desired_image_height", _desired_image_height, 480);
    _nh.param<bool>("color_image", _colorImage, false);
    _nh.param<bool>("compressed_image", _compressedImage, false);
    _nh.param<std::string>("camera_port_name", _camera_port_name, "/dev/video1");
    _nh.param<std::string>("camera_link_frame", _camera_link_frame, "camera_link");
    _nh.param<std::string>("base_link_frame", _base_link_frame, "/base_link");
    _nh.param<double>("translation_x", _translation_x, 0.0);
    _nh.param<double>("translation_y", _translation_y, 0.0);
    _nh.param<double>("translation_z", _translation_z, 0.0);
    _nh.param<double>("rotation_euler_deg_r", _rotation_euler_deg_r, 0.0);
    _nh.param<double>("rotation_euler_deg_p", _rotation_euler_deg_p, 0.0);
    _nh.param<double>("rotation_euler_deg_y", _rotation_euler_deg_y, 0.0);

    // Load camera calibration parameters
    _readCalibrationFile(_nh);

    // Initialize the publisher for the images and camera info
    if (_compressedImage) {
        _compressed_image_publisher = _nh.advertise<sensor_msgs::CompressedImage>("image_compressed", 1);
    } else {
        _image_publisher = _nh.advertise<sensor_msgs::Image>("image_raw", 1);
    }
    _camera_info_publisher = _nh.advertise<sensor_msgs::CameraInfo>("camera_info", 1);

    // Publish static transform
    _publishCameraTransform();

    _fps = std::max(_fps, 1); // Saturate _fps to be at least 1
    _timer = _nh.createTimer(ros::Duration(1.0 / _fps), &RosCameraWrapper::_takePicture, this);
}

void RosCameraWrapper::initializeCamera()
{
    ROS_INFO("Attempting to open video source: %s", _camera_port_name.c_str());
    _cap.open(_camera_port_name);

    if (!_cap.isOpened())
    {
        ROS_ERROR("Unable to open the video source.");
        return;
    }

    _cap.set(cv::CAP_PROP_FRAME_WIDTH, _desired_image_width);
    _cap.set(cv::CAP_PROP_FRAME_HEIGHT, _desired_image_height);
    _cap.set(cv::CAP_PROP_FPS, _fps);

    ROS_INFO("Camera open and set.");
}

void RosCameraWrapper::_readCalibrationFile(const ros::NodeHandle &nh)
{
    _nh.getParam("image_width", _calibrated_image_width);
    _nh.getParam("image_height", _calibrated_image_height);

    if (_calibrated_image_width != _desired_image_width || _calibrated_image_height != _desired_image_height)
    {
        ROS_ERROR("Set image size (%dx%d) mismatch calibration image size (%dx%d).", _desired_image_width, _desired_image_height, _calibrated_image_width, _calibrated_image_height);
    }

    std::vector<double> camera_matrix_data;
    nh.getParam("camera_matrix/data", camera_matrix_data);
    _fx = camera_matrix_data[0];
    _fy = camera_matrix_data[4];
    _cx = camera_matrix_data[2];
    _cy = camera_matrix_data[5];

    nh.getParam("distortion_model", _distortion_model);

    std::vector<double> distortion_coefficients_data;
    nh.getParam("distortion_coefficients/data", distortion_coefficients_data);
    _k1 = distortion_coefficients_data[0];
    _k2 = distortion_coefficients_data[1];
    _p1 = distortion_coefficients_data[2];
    _p2 = distortion_coefficients_data[3];
    _k3 = distortion_coefficients_data[4];

    std::vector<double> projection_matrix_data;
    nh.getParam("projection_matrix/data", projection_matrix_data);
    _pfx = projection_matrix_data[0];
    _pfy = projection_matrix_data[5];
    _pcx = projection_matrix_data[2];
    _pcy = projection_matrix_data[6];
}

void RosCameraWrapper::_publishCameraTransform()
{
    geometry_msgs::TransformStamped static_transformStamped;

    static_transformStamped.header.stamp = ros::Time::now();
    static_transformStamped.header.frame_id = _base_link_frame;
    static_transformStamped.child_frame_id = _camera_link_frame;

    static_transformStamped.transform.translation.x = _translation_x;
    static_transformStamped.transform.translation.y = _translation_y;
    static_transformStamped.transform.translation.z = _translation_z;

    double rotation_euler_r = _rotation_euler_deg_r * M_PI / 180.0;
    double rotation_euler_p = _rotation_euler_deg_p * M_PI / 180.0;
    double rotation_euler_y = _rotation_euler_deg_y * M_PI / 180.0;

    tf2::Quaternion quaternion;
    quaternion.setRPY(rotation_euler_r, rotation_euler_p, rotation_euler_y);

    static_transformStamped.transform.rotation.x = quaternion.x();
    static_transformStamped.transform.rotation.y = quaternion.y();
    static_transformStamped.transform.rotation.z = quaternion.z();
    static_transformStamped.transform.rotation.w = quaternion.w();

    _static_broadcaster.sendTransform(static_transformStamped);
}

void RosCameraWrapper::_takePicture(const ros::TimerEvent &event)
{
    // Acquire an image
    _cap >> _image;

    if (_colorImage) {
        cv::cvtColor(_image, _image, cv::COLOR_BGR2RGB);
    } else {
        cv::cvtColor(_image, _image, cv::COLOR_BGR2GRAY);
    }
    
    // Publish image
    _publish();
}

void RosCameraWrapper::_publish()
{
    // Publish camera image
   if (_compressedImage) {
        _publishCompressedCameraImage();
    } else {
        _publishCameraImage();
    }

    // Publish camera info
    _publishCameraInfo();
}

void RosCameraWrapper::_publishCameraImage()
{
    sensor_msgs::Image image_msg;

    image_msg.header.stamp = ros::Time::now();
    image_msg.header.frame_id = _camera_link_frame;

    image_msg.height = _image.rows;
    image_msg.width = _image.cols;
    if (_colorImage) {
        image_msg.encoding = sensor_msgs::image_encodings::RGB8;
    } else {
        image_msg.encoding = sensor_msgs::image_encodings::MONO8;
    }
    image_msg.step = _image.cols * _image.elemSize();

    size_t data_size = _image.rows * _image.cols * _image.elemSize();
    image_msg.data.resize(data_size);
    std::memcpy(image_msg.data.data(), _image.data, data_size);

    _image_publisher.publish(image_msg);
}

void RosCameraWrapper::_publishCompressedCameraImage()
{
    sensor_msgs::CompressedImage compressed_image_msg;

    compressed_image_msg.header.stamp = ros::Time::now();
    compressed_image_msg.header.frame_id = _camera_link_frame;

    compressed_image_msg.format = "jpeg";

    std::vector<uint8_t> compressed_data;
    cv::imencode("." + compressed_image_msg.format, _image, compressed_data);

    compressed_image_msg.data = compressed_data;

    _compressed_image_publisher.publish(compressed_image_msg);
}

void RosCameraWrapper::_publishCameraInfo()
{
    sensor_msgs::CameraInfo camera_info_msg;

    camera_info_msg.header.stamp = ros::Time::now();
    camera_info_msg.header.frame_id = _camera_link_frame;

    camera_info_msg.height = _calibrated_image_height;
    camera_info_msg.width = _calibrated_image_width;
    camera_info_msg.distortion_model = _distortion_model;
    camera_info_msg.D = {_k1, _k2, _p1, _p2, _k3};
    camera_info_msg.K = {_fx, 0, _cx, 0, _fy, _cy, 0, 0, 1};
    camera_info_msg.R = {1, 0, 0, 0, 1, 0, 0, 0, 1};
    camera_info_msg.P = {_pfx, 0, _pcx, 0, 0, _pfy, _pcy, 0, 0, 0, 1, 0};

    _camera_info_publisher.publish(camera_info_msg);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ros_camera_wrapper");

    RosCameraWrapper ros_camera_wrapper;
    ros_camera_wrapper.initializeCamera();

    ros::spin();

    return 0;
}

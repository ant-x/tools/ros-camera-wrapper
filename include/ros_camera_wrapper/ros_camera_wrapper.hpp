#ifndef ROS_CAMERA_WRAPPER_HPP
#define ROS_CAMERA_WRAPPER_HPP

#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/CameraInfo.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>

class RosCameraWrapper
{
public:
    RosCameraWrapper();

    void initializeCamera();

private:
    // Node handle
    ros::NodeHandle _nh;

    // Parameters
    int _fps;
    std::string _camera_port_name;
    std::string _camera_link_frame;
    std::string _base_link_frame;
    double _translation_x, _translation_y, _translation_z;
    double _rotation_euler_deg_r, _rotation_euler_deg_p, _rotation_euler_deg_y;

    // Publishers
    ros::Publisher _image_publisher;
    ros::Publisher _compressed_image_publisher;
    ros::Publisher _camera_info_publisher;
    tf2_ros::StaticTransformBroadcaster _static_broadcaster;

    // Camera
    cv::VideoCapture _cap;
    cv::Mat _image;
    bool _colorImage;
    bool _compressedImage;
    int _desired_image_width, _desired_image_height;

    // Camera calibration parameters
    int _calibrated_image_width, _calibrated_image_height;
    std::string _distortion_model;
    double _fx, _fy, _cx, _cy;
    double _k1, _k2, _p1, _p2, _k3;
    double _pfx, _pfy, _pcx, _pcy;

    // Load camera calibration parameters from file
    void _readCalibrationFile(const ros::NodeHandle &nh);

    // Publish static transform
    void _publishCameraTransform();

    // Timer for the publish callback
    ros::Timer _timer;
    void _takePicture(const ros::TimerEvent &event);
    void _publish();
    void _publishCameraImage();
    void _publishCompressedCameraImage();
    void _publishCameraInfo();

};

#endif // ROS_CAMERA_WRAPPER_HPP

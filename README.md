# ROS Camera Wrapper
This repository contains a ROS node for interfacing with a camera and publishing images along with camera information. 
The provided launch file and configuration files allow you to customize various parameters such as frame transformations, image size, and camera calibration.

## Launch File
The launch file (`ros_camera_wrapper.launch`) is responsible for starting the ROS node with configurable parameters. Here's an explanation of the launch file:

```bash
<launch>
    <arg name="fps" default="10" />
    <arg name="desired_image_width" default="640" />
    <arg name="desired_image_height" default="480" />
    <arg name="color_image" default="false" />
    <arg name="compressed_image" default="false" />
    <arg name="camera_port_name" default="/dev/video1" />

    <node name="ros_camera_wrapper" pkg="ros_camera_wrapper" type="ros_camera_wrapper" output="screen">
        <!-- Node parameters -->
        <param name="fps" value="$(arg fps)" />
        <param name="desired_image_width" value="$(arg desired_image_width)" />
        <param name="desired_image_height" value="$(arg desired_image_height)" />
        <param name="color_image" value="$(arg color_image)" />
        <param name="compressed_image" value="$(arg compressed_image)" />
        <param name="camera_port_name" value="$(arg camera_port_name)" />

        <!-- Load additional parameters from files -->
        <rosparam command="load" file="$(find ros_camera_wrapper)/params/params.yaml" />
        <rosparam command="load" file="$(find ros_camera_wrapper)/calibration/calibration.yaml" />
    </node>
</launch>
```

Adjust the launch file parameters as needed for your specific camera setup.

## Node Implementation
The C++ implementation (`ros_camera_wrapper.cpp`) is responsible for initializing the camera, reading calibration files, and publishing images and camera information. Key functionalities include:

* **Initializing Camera**: The `initializeCamera` method opens the specified video source and sets desired parameters.

* **Reading Calibration File**: The `_readCalibrationFile` method reads camera calibration parameters from the specified YAML file.

* **Publishing Transform**: The `_publishCameraTransform` method publishes the static transform between the base link and camera link frames.

* **Taking Pictures**: The `_takePicture` method is triggered by a timer to capture images from the camera.

* **Publishing Images and Camera Info**: The `_publish`, `_publishCameraImage`, `_publishCompressedCameraImage`, and `_publishCameraInfo` methods handle publishing images and camera information.

## Calibration and Parameter Files
The `calibration.yaml` file contains camera calibration parameters, and the `params.yaml` file contains additional node parameters. 
Customize these files based on your camera's calibration data and specific requirements.

## Running the Node
To run the ROS node, use the provided launch file:

```bash
roslaunch ros_camera_wrapper ros_camera_wrapper.launch
```
Adjust launch file arguments and configuration files as needed for your setup.

Feel free to modify and extend the code to suit your application's requirements.

## Contributions

Contributions are welcome! For suggestions, improvements, or bug reports, refer to the "Issues" section on GitHub.

## License

This package is distributed under the [BSD-3-Clause License](LICENSE).

## Authors

- Mattia Giurato [mattia@antx.it](mailto:mattia@antx.it)
